package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    private static final Logger LOGGER=LoggerFactory.getLogger(Main.class);

    @RequestMapping("/")
    public String index() {
        LOGGER.info("---");
        LOGGER.info("Login {}, {} and {}", 1,2,3);
        LOGGER.trace("TRACE");
        LOGGER.debug("DEBUG");
        LOGGER.info("INFO");
        LOGGER.warn("WARN");
        LOGGER.error("ERROR");
        LOGGER.info("---");
        return "hello";


    }

}